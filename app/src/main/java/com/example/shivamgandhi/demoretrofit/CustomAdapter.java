package com.example.shivamgandhi.demoretrofit;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import static com.example.shivamgandhi.demoretrofit.R.id.textView4;

/**
 * Created by Shivam on 10/11/2017.
 */

class CustomAdapter extends BaseAdapter {

    ArrayList<Contact> arryList;
    Context context;

    public CustomAdapter(Context retrofit, ArrayList<Contact> arryList) {

        this.arryList=arryList;
        context=retrofit;

    }

    @Override
    public int getCount() {
        return arryList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

      //  Contact contact = arryList.get(position);

      //  View v;
           // LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //v = layoutInflater.inflate(R.layout.custom_5, parent,false);
      //  v = View.inflate(context,R.layout.custom_5,null);
      /*      TextView tv1 = v.findViewById(R.id.textView4);
            TextView tv2 = v.findViewById(R.id.textView5);
            TextView tv3 = v.findViewById(R.id.textView6);

            tv1.setText(contact.getName());
            tv2.setText(contact.getEmail());
            tv3.setText(contact.getAddress());


         return v;
*/

        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(context).inflate(R.layout.custom_5,parent,false);

        Contact contact = arryList.get(position);
        TextView tv1 = listItem.findViewById(R.id.textView4);
        TextView tv2 = listItem.findViewById(R.id.textView5);
        TextView tv3 = listItem.findViewById(R.id.textView6);

        tv1.setText(contact.getName());
        tv2.setText(contact.getEmail());
        tv3.setText(contact.getAddress());

        return listItem;

    }
}
