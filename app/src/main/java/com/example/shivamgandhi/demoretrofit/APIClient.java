package com.example.shivamgandhi.demoretrofit;

import retrofit2.*;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Shivam on 10/11/2017.
 */

public class APIClient {

    public static final String URLDATA ="https://jsonplaceholder.typicode.com/";

    public static retrofit2.Retrofit getClient()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URLDATA)
                .addConverterFactory(GsonConverterFactory.create()).build();
        return  retrofit;
    }
}

//[] JSONArray ArrayList<>

//{} JSONObject Object(Class)
