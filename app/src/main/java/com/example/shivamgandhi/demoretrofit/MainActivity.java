package com.example.shivamgandhi.demoretrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ArrayList<Contact> arryList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        listView = (ListView)findViewById(R.id.listview_retrofit);
        arryList = new ArrayList<>();
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<List<Contact>> call = apiInterface.getUser();
        call.enqueue(new Callback<List<Contact>>() {

            @Override
            public void onResponse(Call<List<Contact>> call, Response<List<Contact>> response) {

                arryList = (ArrayList<Contact>) response.body();
                CustomAdapter customAdapter = new CustomAdapter(MainActivity.this,arryList);
                listView.setAdapter(customAdapter);

            }

            @Override
            public void onFailure(Call<List<Contact>> call, Throwable t) {

            }
        });

    }
}
