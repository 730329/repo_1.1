package com.example.shivamgandhi.demoretrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Shivam on 10/11/2017.
 */

public interface APIInterface {

    @GET("posts")
    Call<List<Contact>> getUser();

}
